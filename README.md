# Political Archive Open-Source Website

Welcome to the GitLab for Political Archive.

You can download this repo by either doing it manually with the download button or use the `git` command.

If you have git installed on your machine, open up whatever commandline of your choice and type:

```
git clone https://gitlab.com/politicalarchive/political-archive-open-source-website.git
```

View the site live at https://politicalarchive.neocities.org/.

Enjoy!